<?php

/*
Plugin Name: Guru No-index pages listing
Plugin URI: https://affprojects.com
Description: List pages that have no-index metatag set.
Version: 1.0
Author: Paulius Pazdrazdys
Author URI: https://internetosvetaines.com
*/

if ( ! class_exists( 'WP_List_Table' ) ) {
  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Noindex_Pages_List extends WP_List_Table {

  /** Class constructor */
  public function __construct() {

    parent::__construct( [
      'singular' => __( 'Guru Noindex Page', 'guru-noindex-pages' ), //singular name of the listed records
      'plural'   => __( 'Guru Noindex Pages', 'guru-noindex-pages' ), //plural name of the listed records
      'ajax'     => false //does this table support ajax?
    ] );

  }


  /**
   * Retrieve customers data from the database
   *
   * @param int $per_page
   * @param int $page_number
   *
   * @return mixed
   */
  public static function get_customers( $per_page = 5, $page_number = 1 ) {

    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}posts";
    $sql .= " INNER JOIN {$wpdb->prefix}postmeta m1
    ON ( {$wpdb->prefix}posts.ID = m1.post_id )";

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    }

    $sql .= " WHERE
    {$wpdb->prefix}posts.post_type IN ('post', 'page')
    AND {$wpdb->prefix}posts.post_status = 'publish'
    AND ( m1.meta_key = '_yoast_wpseo_meta-robots-noindex' AND m1.meta_value = '1' )
    GROUP BY {$wpdb->prefix}posts.ID";

    $sql .= " LIMIT $per_page";
    $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

    $result = $wpdb->get_results( $sql, 'ARRAY_A' );

    return $result;
  }

  /**
   * Returns the count of records in the database.
   *
   * @return null|string
   */
  public static function record_count() {
    global $wpdb;

    $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}posts";
    $sql .= " INNER JOIN {$wpdb->prefix}postmeta m1
    ON ( {$wpdb->prefix}posts.ID = m1.post_id )";

    $sql .= " WHERE
    {$wpdb->prefix}posts.post_type IN ('post', 'page')
    AND {$wpdb->prefix}posts.post_status = 'publish'
    AND ( m1.meta_key = '_yoast_wpseo_meta-robots-noindex' AND m1.meta_value = '1' )";

    return $wpdb->get_var( $sql );
  }


  /** Text displayed when no customer data is available */
  public function no_items() {
    _e( 'No pages.', 'guru-noindex-pages' );
  }


  /**
   * Render a column when no column specific method exist.
   *
   * @param array $item
   * @param string $column_name
   *
   * @return mixed
   */
  public function column_default( $item, $column_name ) {
    switch ( $column_name ) {
      case 'post_title':
        $post_link = get_permalink($item['ID']);
        $post_link_markup = '<a href="' . $post_link . '">' . $item['post_title'] . '</a>';
        return $post_link_markup;
      case 'post_slug':
        $post_slug = str_replace(home_url(), '', get_permalink($item['ID']));
        return $post_slug;
      case 'post_status':
        $post_status = get_post_status($item['ID']);
        $post_status = $post_status == 'publish' ? 'Published' : $post_status;
        return $post_status;
      case 'post_date':
        $post_date = get_the_date('Y.m.d H:i', $item['ID']);
        return $post_date;
      case 'post_author':
        $author_id = $item['post_author'];
        $post_author = get_the_author_meta('display_name' , $author_id);
        return $post_author;
      case 'post_type':
        $post_type_obj = get_post_type_object(get_post_type($item['ID']));
        $post_type_name = $post_type_obj->labels->singular_name;
        return $post_type_name;
      case 'post_edit':
        $post_edit_link = get_edit_post_link($item['ID']);
        return '<a href="' . $post_edit_link . '">Edit</a>';
      default:
      return '';
        // return print_r( $item, true ); //Show the whole array for troubleshooting purposes
    }
  }

  /**
   * Render the bulk edit checkbox
   *
   * @param array $item
   *
   * @return string
   */
  function column_cb( $item ) {
    return sprintf(
      //'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
      ''
    );
  }


  /**
   * Method for name column
   *
   * @param array $item an array of DB data
   *
   * @return string
   */
  function column_name( $item ) {

    $title = '<strong>' . $item['post_title'] . '</strong>';

    return $title;
  }


  /**
   *  Associative array of columns
   *
   * @return array
   */
  function get_columns() {
    $columns = [
      'post_title'    => __('Content Title', 'guru-noindex-pages'),
      'post_slug'     => __('Post URL', 'guru-noindex-pages'),
      'post_date'     => __('Content Creation Date', 'guru-noindex-pages'),
      'post_author'   => __('Content Author', 'guru-noindex-pages'),
      'post_type'     => __('Content Type', 'guru-noindex-pages'),
      'post_status'   => __('Content Status', 'guru-noindex-pages'),
      'post_edit'     => __('Content Edit', 'guru-noindex-pages')
    ];

    return $columns;
  }


  /**
   * Columns to make sortable.
   *
   * @return array
   */
  public function get_sortable_columns() {
    $sortable_columns = array(
      'name' => array( 'name', true ),
      'city' => array( 'city', false )
    );

    return $sortable_columns;
  }


  /**
   * Handles data query and filter, sorting, and pagination.
   */
  public function prepare_items() {

    $this->_column_headers = $this->get_column_info();

    /** Process bulk action */
    $this->process_bulk_action();

    $per_page     = $this->get_items_per_page( 'list_items_per_page', 5 );
    $current_page = $this->get_pagenum();
    $total_items  = self::record_count();

    $this->set_pagination_args( [
      'total_items' => $total_items, //WE have to calculate the total number of items
      'per_page'    => $per_page //WE have to determine how many items to show on a page
    ] );

    $this->items = self::get_customers( $per_page, $current_page );
  }

}


class Guru_Noindex_Pages_Plugin {

  // class instance
  static $instance;

  // customer WP_List_Table object
  public $noindex_pages_obj;

  // class constructor
  public function __construct() {
    add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
    add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
  }


  public static function set_screen( $status, $option, $value ) {
    return $value;
  }

  public function plugin_menu() {

    $hook = add_menu_page(
      'Guru Noindex Content',
      'Guru Noindex Content',
      'edit_pages',
      'guru_noindex_content',
      [ $this, 'plugin_settings_page' ]
    );

    add_action( "load-$hook", [ $this, 'screen_option' ] );

  }


  /**
   * Plugin settings page
   */
  public function plugin_settings_page() {
    ?>
    <style>
      .tablenav {
        display: none;
      }
    </style>
    <div class="wrap">
      <h2>Guru Noindex Content</h2>
      <p>Pages that have <strong>&lt;meta name="robots" content="noindex"/></strong> Metatag.</p>
      <p>Good for reviewing pages/posts that are no-indexed.</p>

      <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
          <div id="post-body-content">
            <div class="meta-box-sortables ui-sortable">
              <form method="post">
                <?php
                $this->noindex_pages_obj->prepare_items();
                $this->noindex_pages_obj->display(); ?>
              </form>
            </div>
          </div>
        </div>
        <br class="clear">
      </div>
    </div>
  <?php
  }

  /**
   * Screen options
   */
  public function screen_option() {

    $option = 'per_page';
    $args   = [
      'label'   => 'Items',
      'default' => 50,
      'option'  => 'list_items_per_page'
    ];

    add_screen_option( $option, $args );

    $this->noindex_pages_obj = new Noindex_Pages_List();
  }


  /** Singleton instance */
  public static function get_instance() {
    if ( ! isset( self::$instance ) ) {
      self::$instance = new self();
    }

    return self::$instance;
  }

}


add_action( 'plugins_loaded', function () {
  Guru_Noindex_Pages_Plugin::get_instance();
} );
